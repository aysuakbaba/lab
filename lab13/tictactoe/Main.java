package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws tictactoe.InvalidMoveException {
        Scanner reader = new Scanner(System.in);

        tictactoe.Board board = new tictactoe.Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();
            boolean invalidRow=false;
            boolean invalidColumn=false;
            int row=0;
            int col =0;
            do {
                System.out.print("Player " + player + " enter row number:");
                try {
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid ınteger");
                    invalidRow=false;
                } catch (NumberFormatException ex){
                    System.out.println("Invalid integer");
                    invalidRow=true;
                }
            }while(invalidRow);

            do{
                System.out.print("Player " + player + " enter column number:");
                try{
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid intger");
                    invalidColumn=false;
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid integer");
                    invalidColumn=true;
                }
            }while(invalidColumn);
            try {


                board.move(row, col);
            }catch(tictactoe.InvalidMoveException e){
                System.out.println(e.getMessage());
            }
            System.out.println(board);
        }


        reader.close();



    }

