public class GuessNumber {import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

    public class GuessNumber {

        public static void main(String[] args) throws IOException {
            Scanner reader = new Scanner(System.in); //Creates an object to read user input
            Random rand = new Random(); //Creates an object from Random class
            int number =rand.nextInt(100); //generates a number between 0 and 99


            System.out.println("Hi! I'm thinking of a number between 0 and 99.");
            System.out.print("Can you guess it: ");

            int guess = reader.nextInt(); //Read the user input
            int sayac = 0;

            while ( guess != number && guess != -1){
                System.out.println("sorry");
                if (guess > number)
                    System.out.println("Your guess is greater than the number");
                else if (guess < number)
                    System.out.println("Your guess is less than the number");
                System.out.println("Type -1 to quit or guess another: ");
                guess = reader.nextInt();
                sayac += 1;
            }

            if (guess == number)
                System.out.println("Congratulations! yo wwon after " + sayac + "attempts");
            else
                System.out.println("sorry the number was " + number);



            reader.close(); //Close the resource before exiting
        }


    }
}
